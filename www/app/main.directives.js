(function (ang) {
	
	var app = ang.module('wtp');

	app.directive('wtpAppContainer', function () {
		return {
			restrict: 'E',
			templateUrl: 'app/main.html'
		}
	});

})( angular );
# WTP Ionic

## Requirements
- Node
- NPM
- Ionic
- Bower

## Installation
```bash
git clone https://dsilva2401x@bitbucket.org/dsilva2401x/wtp-ionic.git wtp-ionic
cd wtp-ionic
npm install
bower install
```

## Reference
[Ionic](http://ionicframework.com/docs/)
